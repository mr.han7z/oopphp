<?php

require_once('animal.php');

class frog extends animal {
    public $name ;
    public $legs = 4;
    public function jump(){
        echo "Hop Hop";
    }

}

/* index.php
$sungokong = new Ape("kera sakti");
$sungokong->yell() // "Auooo"

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"*/